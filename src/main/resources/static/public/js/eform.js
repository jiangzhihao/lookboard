var flow_part1 = {
	flow1 : '需求提出',
	flow2 : '需求提出部门领导审批',
	flow3 : '设计部门领导指定审批人',
	flow4 : '设计部门项目负责人评审',
}

$(function() {
	Table.eFormTable.init();
	Modal.EformModal.init();
	Modal.clauseModal.init();
	$('#searchBtn').click(function() {
		Table.eFormTable.reload();
	});
	$('#resetBtn').click(function() {
		$('#search_code').val('');
		$('#search_businessName').val('');
		$('#search_stateNow').val('');
		Table.eFormTable.reload();
	});
})

var Table = {
	eFormTable : {
		init : function() {
			$('#ele-forms').bootstrapTable({
				url : '/eform',
				columns : [ {
					field : 'createTime',
					title : '创建时间',
					width : '11%'
				}, {
					field : 'code',
					title : '申请单号',
					width : '11%'
				}, {
					field : 'businessName',
					title : '业务名称',
					width : ''
				}, {
					field : 'businessType',
					title : '业务类型',
					width : '8%'
				}, {
					field : 'proposer',
					title : '申请人ID',
					width : '15%'
				}, {
					field : 'stateNow',
					title : '当前环节',
					width : '17%'
				} ],
				pagination : true, // 是否显示分页（*）
				sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
				pageNumber : 1, // 初始化加载第一页，默认第一页
				pageSize : 15, // 每页的记录行数（*）
				pageList : [ 10, 15, 20, 25 ], // 可供选择的每页的行数（*）
				queryParams : function(params) {
					var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
						limit : params.limit, // 页面大小
						offset : params.offset,
						code : $('#search_code').val(),
						businessName : $('#search_businessName').val(),
						stateNow : $('#search_stateNow').val()
					};
					return temp;
				},
				onLoadSuccess : function(data) {
					var trs = $('#ele-forms tr')
					if (null != trs && trs.length > 0) {
						for (var i = 0; i < trs.length; i++) {
							var tr = $(trs[i]);
							tr.attr('data-toggle', 'modal');
							tr.attr('data-target', '#ele-form');
						}
					}
					var unsetBox = $("#ele-forms thead tr:first");
					if (null != unsetBox) {
						unsetBox.attr('data-toggle', '');
						unsetBox.attr('data-target', '');
					}
				},
				onClickRow : function(row, tr) { // 设置 标签组的内容、点击事件；请求数据、展示数据
					var id = row.id;
					var businessName = row.businessName;
					Modal.EformModal.show(id)
				}
			});
		},
		reload : function() {
			$('#ele-forms').bootstrapTable('refresh');
		}
	},
	
	clauseTable : {
		init : function(eform_id) {
			$('#clause-list-form').bootstrapTable({
				url : '/clause/p/' + eform_id,
				columns : [ {
					field : 'code',
					title : '条目编号',
					width : '11%'
				}, {
					field : 'subject',
					title : '条目主题',
					width : '25%'
				}, {
					field : 'dept',
					title : '提出部门',
					width : '11%'
				}, {
					field : 'system',
					title : '系统、版本',
					width : '11%'
				}, {
					field : 'zhInput',
					title : '中汇投入量（人/天）',
					width : '11%'
				}, {
					field : 'status',
					title : '回退状态',
					width : '11%'
				}, {
					field : 'checkStatus',
					title : '验收状态',
					width : '11%'
				}],
				pagination : true, // 是否显示分页（*）
				sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
				pageNumber : 1, // 初始化加载第一页，默认第一页
				pageSize : 5, // 每页的记录行数（*）
				pageList : [ 5, 10, 15, 20], // 可供选择的每页的行数（*）
				queryParams : function(params) {
					var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
						limit : params.limit, // 页面大小
						offset : params.offset
					};
					return temp;
				},
				onLoadSuccess : function(data) {
					var row = data.rows[0];
					var id = row.id;
					$.ajax({
						url : '/clause/' + id,
						data : {time : new Date},
						dataType : 'json',
						method : 'get',
						success : function(result) {
							Modal.clauseModal.clear();
							Modal.clauseModal.setValue(result);
						}
					});
				},
				onClickRow : function(row, tr) { // 设置 标签组的内容、点击事件；请求数据、展示数据
					var id = row.id;
					$.ajax({
						url : '/clause/' + id,
						data : {time : new Date},
						dataType : 'json',
						method : 'get',
						success : function(result) {
							Modal.clauseModal.clear();
							Modal.clauseModal.setValue(result);
						}
					});
				}
			});
		},
		
		reload : function(eformid) {
			$("#clause-list-form").bootstrapTable('destroy'); 
			Table.clauseTable.init(eformid);
			var opt = {
			         url: '/clause/p/' + eformid //重新请求的链接
			        };
			$("#clause-list-form").bootstrapTable('refresh', opt);
		}
	}
}


var Modal = {
		EformModal : {
			init : function() {
				$('#ele-form').on('hidden.bs.modal', function() {
					Modal.EformModal.clear();
					setTimeout(function(){ //关闭1模态框会使body滚动失效
				        $('body').addClass('modal-open');
				        $('body').css('padding-right','17px');
				    },500);
				});
			},
			show : function(eformId) {
				Modal.EformModal.clear(); //清除modal内容
				$.ajax({ // 将电子需求单的详细信息加载到表单
					url : "/eform/" + eformId,
					data : {
						time : new Date()
					},
					dataType : 'json',
					method : 'get',
					success : function(result) {
						loadEformMessage(result);
					}
				});
				$.ajax({ //将 子对象的数量加载到界面
					url : "eform/2/" + eformId,
					data : {
						time : new Date()
					},
					dataType : 'json',
					method : 'get',
					success : function(result) {
						$('#clause-count').text(result.clauseCount);
						$('#workload-count').text(result.workloadCount);
						$('#plan-count').text(result.workloadCount);
					}
				});
			},
			clear : function() {
				$('#ele-form input').each(function(index, ele) {
					$(ele).val('');
				})
				$('.btn-count').each(function(index, ele) {
					$(ele).text('0');
				})
			}
		},
		
		clauseModal : {
			init : function() {
				$('#clause-list').on('hidden.bs.modal', function() {
					var eformId = $('#form-id').val();
					Modal.EformModal.show(eformId);
					$('#ele-form').modal('show'); //由于并非是点击事件，所以要手写modal展示
					Modal.clauseModal.clear();//情况模态框
				});
			},
			show : function(eformid) {
				Table.clauseTable.reload(eformid);
				$('#ele-form').modal('hide');
				$('#clause-list').modal('show');
				$('#form-id').val(eformid);
			},
			clear : function() {
				$('#clause-form input').each(function(index, ele) {
					$(ele).val('');
				})
			},
			setValue : function(clause) {
				jQuery.each(clause, function(i, val) {
					var box = $('#clause-' + i);
					if (null != box) {
						box.val(val);
					}
				});
			}
		}
}



/**
 * 跳转到 条目拆分、工作量评估
 * @param btnName
 * @returns
 */
function sonsBtnClick(btnName) {
	var eformid = $('#id').val();
	if ('clause-btn' == btnName) {
		Modal.clauseModal.show(eformid);
	}
}

/**
 * 将电子需求单内容加载到表单中
 * 
 * @param eForm
 * @returns
 */
function loadEformMessage(eForm) {
	jQuery.each(eForm, function(i, val) {
		var box = $('#' + i);
		if (null != box) {
			box.val(val);
		}
	});
	$('#eform-title').text(eForm.businessName);
}

