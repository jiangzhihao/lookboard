var myChart_1 = echarts.init(document.getElementById('main_1'));
var myChart_2 = echarts.init(document.getElementById('main_2'));

$(function() {
	initSec1(myChart_1);
	initSec2(myChart_2);
	initBtnEvent();

});

function initBtnEvent() {
	var btns = $(".echart-pie-load-btn");
	$.each(btns, function(index, value) {
		$(value).click(function() {
			loadBdmPie(myChart_1, $(value).text());
		});
	});
}

/**
 * 初始化饼状图
 * 
 * @returns
 */
function initSec1(myChart_1) {
	// 指定图表的配置项和数据
	option_1 = {
		title : {
			text : '技术开发部 预算使用情况',
			subtext : '预算总量：1618',
			x : 'center'
		},
		tooltip : {
			trigger : 'item',
			formatter : "{a} <br/>{b} : {c} ({d}%)"
		},
		series : [ {
			name : '状态',
			type : 'pie',
			radius : '65%',
			center : [ '50%', '50%' ],
			selectedMode : 'single',
			data : [ {
				value : 1269.1797,
				name : '可用量',
			}, {
				value : 280.6812,
				name : '已用量'
			}, {
				value : 68.139,
				name : '冻结量'
			} ],
			itemStyle : {
				emphasis : {
					shadowBlur : 10,
					shadowOffsetX : 0,
					shadowColor : 'rgba(0, 0, 0, 0.5)'
				}
			}
		} ]
	};
	// 使用刚指定的配置项和数据显示图表。
	myChart_1.setOption(option_1);
	loadBdmPie(myChart_1, '中心财务部');
}

function loadBdmPie(myChart_1, dept) {
	myChart_1.showLoading();
	$('#menuLook').text(dept);
	$.ajax({
		url : '/bdm/echart',
		dataType : 'json',
		method : 'get',
		data : {
			time : new Date(),
			dept : dept
		},
		success : function(data) {
			console.info(data);
			myChart_1.hideLoading();
			myChart_1.setOption({
				title : {
					text : data.dept + ' 预算使用情况',
					subtext : '预算总量：' + data.yszl,
					x : 'center'
				},
				series : [ {
					data : [ {
						value : data.kyl,
						name : '可用量',
					}, {
						value : data.yyl,
						name : '已用量'
					}, {
						value : data.djl,
						name : '冻结量'
					} ]
				} ]

			});
		}
	});
}

/**
 * 初始化折线图
 * 
 * @returns
 */
function initSec2(myChart_2) {
	option_2 = {
		xAxis : {
			data : []
		},
		yAxis : {},
		series : [ {
			type : 'line',
			data : []
		} ]
	};
	myChart_2.setOption(option_2);
	myChart_2.showLoading();
	$.ajax({
		url : '/eform/echart',
		dataType : 'json',
		method : 'get',
		data : {
			time : new Date(),
			rows : 14
		},
		success : function(data) {
			myChart_2.hideLoading();
			myChart_2.setOption({
				xAxis : {
					data : data.x_data
				},
				series : [ {
					name : '新建数量',
					data : data.y_data
				} ]
			});
		}
	});
}