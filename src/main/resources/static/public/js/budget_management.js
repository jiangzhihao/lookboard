

$(function() {
	initTable();
})

/**
 * 初始化 电子需求单列表
 * 
 * @returns
 */
function initTable() {
	$('#bdm-table').bootstrapTable({
		url : '/bdm',
		columns : [ {
			field : 'title',
			title : '标题',
		}, {
			field : 'author',
			title : '创建人',
		}, {
			field : 'dept',
			title : '部门',
		}, {
			field : 'createTime',
			title : '创建时间',
		}, {
			field : 'budget',
			title : '预算人月单价',
		}, {
			field : 'budgetAll',
			title : '预算总量',
		}, {
			field : 'budgetCan',
			title : '可用量',
		} , {
			field : 'budgetLock',
			title : '冻结量',
		} , {
			field : 'budgetHave',
			title : '已用量',
		} , {
			field : 'signCode',
			title : '签报编号',
		} , {
			field : 'signTime',
			title : '签报批准时间',
		} , {
			field : 'eFormCount',
			title : '电子需求单数量',
		} ],
		pagination : true, // 是否显示分页（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 15, // 每页的记录行数（*）
		pageList : [ 10, 15, 20, 25 ], // 可供选择的每页的行数（*）
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				limit : params.limit, // 页面大小
				offset : params.offset
			};
			return temp;
		},
		onLoadSuccess : function(data) {
			if(null != data) {
				var first = data.rows[0];
				$("#bdm-table tbody tr:first").addClass('success');
				initUseBdmTable(first.title);
			}
			
		},
		onClickRow : function(row, tr) { // 设置 标签组的内容、点击事件；请求数据、展示数据
			$('.success').removeClass('success');//去除之前选中的行的，选中样式
			$(tr).addClass('success');
			$("#bdm-use-table").bootstrapTable('destroy'); 
			initUseBdmTable(row.title);
		}
	});
}


function initUseBdmTable(bdm_id) {
	$('#bdm-use-table').bootstrapTable({
		url : '/eform/bdm/'+bdm_id,
		columns : [ {
			field : 'sqdh',
			title : '单号',
		}, {
			field : 'ywmc',
			title : '业务名称',
		}, {
			field : 'zhgzl',
			title : '中汇投入量',
		}, {
			field : 'wbgzl',
			title : '外包投入量',
		}, {
			field : 'trgzl',
			title : '总投入量',
		}, {
			field : 'budgetAll',
			title : '回退量',
		}, {
			field : 'stateNow',
			title : '当前环节'
		}],
		pagination : true, // 是否显示分页（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 15, // 每页的记录行数（*）
		pageList : [ 10, 15, 20, 25 ], // 可供选择的每页的行数（*）
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				limit : params.limit, // 页面大小
				offset : params.offset
			};
			return temp;
		},
		onLoadSuccess : function(data) {
			
		},
		onClickRow : function(row, tr) { // 设置 标签组的内容、点击事件；请求数据、展示数据
			$('.success').removeClass('success');//去除之前选中的行的，选中样式
			$(tr).addClass('success');
		}
	});
}

