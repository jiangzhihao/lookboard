package com.example.board.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.board.dao.UserDao;
import com.example.board.entity.User;

/**
 * 
 * @author Jason_Jiang
 *
 */
@Service
public class UserService {

	@Autowired
	private UserDao userDao;
	
	/**
	 * 根据id获取用户
	 * @param ids
	 * @return
	 */
	public List<User> getUsersByIds(List<String> ids) {
		List<User> users = new ArrayList<>();
		if(null != ids && ids.size() > 0) {
			users = userDao.getUsersByIds(ids);
		}
		return users;
	}
	
	/**
	 * 将User集合转换成名字字符串
	 * @param users
	 * @return
	 */
	protected String asString(Collection<User> users) {
		String result = "";
		if(null != users && users.size() > 0) {
			StringBuilder sber = new StringBuilder();
			sber.append("");
			for(User user : users) {
				sber.append(user.getName()).append(";"); 
			}
			result = sber.toString();
		}
		if(result.length()>0) {
			result = result.substring(0, result.length()-1);
		}
		return result;
	}
	
}
