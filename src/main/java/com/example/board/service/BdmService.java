package com.example.board.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.board.dao.BdmDao;
import com.example.board.entity.BudgetManagement;

/**
 * 
 * @author yaojunsheng
 *
 */
@Service
public class BdmService {
	
	@Autowired
	private BdmDao bdmDao;
	
	/**
	 * 
	 * @param page
	 * @param rows
	 * @return
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> getBdms(int page, int rows) {
		Map<String, Object> result = new HashMap<>();
		List<BudgetManagement> eleForms = bdmDao.getBdms(page, rows);
		int total = bdmDao.getBdmsCount();
		result.put("total", total);
		result.put("rows", eleForms);
		return result;
	}
	
	/**
	 * 
	 * @param title
	 * @return
	 */
	public Map<String, Object> getBdmByDept(String dept) {
		return bdmDao.getBdmByDept(dept);
	}

}
