package com.example.board.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.board.dao.ClauseDao;
import com.example.board.dao.EleFormDao;
import com.example.board.dao.WorkloadDao;
import com.example.board.entity.EleForm;
import com.example.board.util.ArrayUtil;

/**
 * 
 * @author JZH
 *
 */
@Service
public class EleFormService {

	@Autowired
	private EleFormDao eleFormDao;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ClauseDao clauseDao;
	
	@Autowired
	private WorkloadDao workloadDao;
	
	/**
	 * 分页获取电子需求单列表
	 * @param page
	 * @param rows
	 * @return
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> getEleForms(String code, String businessName, String stateNow, int page, int rows) {
		Map<String, Object> result = new HashMap<>();
		code = "%" + code + "%";
		businessName = "%" +businessName+ "%";
		List<EleForm> eleForms = eleFormDao.getEleForms(code, businessName, stateNow, page, rows);
		int total = eleFormDao.getEleFormsCount(code, businessName, stateNow);
		result.put("total", total);
		result.put("rows", eleForms);
		return result;
	}
	
	/**
	 * 
	 * @param rows
	 * @return
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> createCharts(int rows) {
		List<Map<String, Object>> dbresult = eleFormDao.createCharts(rows);
		Map<String, String> db_date = new HashMap<>();
		for(int i=0;i<dbresult.size();i++) {
			Map<String, Object> map = dbresult.get(i);
			db_date.put(map.get("createTime").toString(), map.get("count").toString());
		}
		
		String[] x_data = new String[rows];
		Integer[] y_data = new Integer[rows];
		LocalDateTime now = LocalDateTime.now();
		for(int i=0;i<rows;i++) {
			String time = now.format(DateTimeFormatter.ISO_DATE);
			int num = (null == db_date.get(time)) ? 0 : Integer.parseInt(db_date.get(time));
			x_data[i] = time.substring(5, time.length());
			y_data[i] = num;
			now = now.minusHours(24);
		}
		ArrayUtil.reverse(x_data);
		ArrayUtil.reverse(y_data);
		Map<String, Object> result = new HashMap<>();
		result.put("x_data", x_data);
		result.put("y_data", y_data);
		return result;
	}
	
	/**
	 * 根据ID获取电子需求单详细信息
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = true)
	public EleForm getEleFormById(String id) {
		EleForm eForm = eleFormDao.getEleFormById(id);
		eForm.setDemandEstimator(userService.asString(userService.getUsersByIds(Arrays.asList(eForm.getDemandEstimator().split(";")))));
		eForm.setDevelopEstimator(userService.asString(userService.getUsersByIds(Arrays.asList(eForm.getDevelopEstimator().split(";")))));
		eForm.setOperationEstimator(userService.asString(userService.getUsersByIds(Arrays.asList(eForm.getOperationEstimator().split(";")))));
		eForm.setDesignEstimator(userService.asString(userService.getUsersByIds(Arrays.asList(eForm.getDesignEstimator().split(";")))));
		eForm.setGrobalLeader(userService.asString(userService.getUsersByIds(Arrays.asList(eForm.getGrobalLeader().split(";")))));
		return eForm;
	}
	
	/**
	 * 获取电子需求单对应的 需求条目、工作量评估数量
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> getChildsCount(String id) {
		Map<String, Object> map = new HashMap<>();
		map.put("clauseCount", clauseDao.getClausesByParentCount(id));
		map.put("workloadCount", workloadDao.getWorkloadsByParentCount(id));
		return map;
	}
	
	/**
	 * 根据预算管理获取需求单
	 * @param id
	 * @param page
	 * @param rows
	 * @return
	 */
	public Map<String, Object> getFormsByBdm(String id, int page, int rows) {
		Map<String, Object> map = new HashMap<>();
		map.put("total",eleFormDao.getFormsCountByBdm(id));
		map.put("rows", eleFormDao.getFormsByBdm(id, page, rows));
		return map;
	}
	
	
	
}
