package com.example.board.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.board.dao.ClauseDao;
import com.example.board.entity.Clause;

/**
 * 
 * @author JZH
 *
 */
@Service
public class ClauseService {

	@Autowired
	private ClauseDao clauseDao;
	
	/**
	 * 分页根据电子需求单获取对应的条目列表
	 * @param parent 电子需求单ID
	 * @param page
	 * @param rows
	 * @return
	 */
	@Transactional(readOnly = true)
	public Map<String, Object> getClausesByParent(String parent, int page, int rows) {
		Map<String, Object> result = new HashMap<>();
		List<Clause> clauses = clauseDao.getClausesByParent(parent, page, rows);
		int total = clauseDao.getClausesByParentCount(parent);
		result.put("total", total);
		result.put("rows", clauses);
		return result;
	}
	
	@Transactional(readOnly = true)
	public Clause getClauseById(String id) {
		return clauseDao.getClauseById(id);
	}
	
	
	
}
