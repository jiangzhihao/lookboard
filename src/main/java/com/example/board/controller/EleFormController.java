package com.example.board.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.board.entity.EleForm;
import com.example.board.service.EleFormService;

/**
 * 
 * @author Jason_Jiang
 *
 */
@RestController
public class EleFormController {

	@Autowired
	private EleFormService eleFormService;

	@GetMapping("eform")
	public Map<String, Object> getEleForms(
			@RequestParam(value = "code", required = false, defaultValue = "") String code,
			@RequestParam(value = "businessName", required = false, defaultValue = "") String businessName,
			@RequestParam(value = "stateNow", required = false, defaultValue = "") String stateNow, Integer limit,
			Integer offset) {
		return eleFormService.getEleForms(code, businessName, stateNow, offset, limit);
	}

	@GetMapping("eform/{id}")
	public EleForm getEleFormById(@PathVariable("id") String id) {
		return eleFormService.getEleFormById(id);
	}

	/**
	 * 根据预算获取对应的需求单
	 * 
	 * @param id 预算管理的ID
	 * @return
	 */
	@GetMapping("eform/bdm/{id}")
	public Map<String, Object> getFormsByBdm(@PathVariable("id") String id, Integer limit, Integer offset) {
		return eleFormService.getFormsByBdm(id, offset, limit);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("eform/2/{id}")
	public Map<String, Object> getChildsCount(@PathVariable("id") String id) {
		return eleFormService.getChildsCount(id);
	}

	/**
	 * 
	 * @param rows
	 * @return
	 */
	@GetMapping("eform/echart")
	public Map<String, Object> createCharts(int rows) {
		return eleFormService.createCharts(rows);
	}

}
