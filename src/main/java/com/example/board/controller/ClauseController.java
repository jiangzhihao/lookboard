package com.example.board.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.board.entity.Clause;
import com.example.board.service.ClauseService;

/**
 * 
 * @author JZH
 *
 */
@RestController
public class ClauseController {
	
	@Autowired
	private ClauseService clauseService;

	/**
	 * 分页获取电子需求单的条目
	 * @param parent
	 * @param limit
	 * @param offset
	 * @return
	 */
	@GetMapping("clause/p/{parent}")
	public Map<String, Object> getClausesByParent(@PathVariable("parent") String parent, Integer limit, Integer offset) {
		return clauseService.getClausesByParent(parent, offset, limit);
	}
	
	@GetMapping("clause/{id}")
	public Clause getClauseById(@PathVariable("id") String id) {
		return clauseService.getClauseById(id);
	}
	
	
	
}
