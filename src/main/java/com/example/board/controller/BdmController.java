package com.example.board.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.board.service.BdmService;

@RestController
public class BdmController {

	@Autowired
	private BdmService bdmService;
	
	/**
	 * 分页获取电子需求单的条目
	 * @param parent
	 * @param limit
	 * @param offset
	 * @return
	 */
	@GetMapping("bdm")
	public Map<String, Object> getBdm(Integer limit, Integer offset) {
		return bdmService.getBdms(offset, limit);
	}
	
	@GetMapping("bdm/echart")
	public Map<String, Object> getBdmByDept(String dept) {
		return bdmService.getBdmByDept(dept);
	}
	
}
