package com.example.board.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.board.util.ResultMessage;

/**
 * 统一异常处理
 * @author JZH
 *
 */
@ControllerAdvice
public class ExceptionController {

    @ResponseBody
    @ExceptionHandler({RuntimeException.class})
    public ResultMessage<?> error(HttpServletRequest request, RuntimeException e) {
        e.printStackTrace();
        return ResultMessage.error("500", e.getMessage());
    }
	
}
