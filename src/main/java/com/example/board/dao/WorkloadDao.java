package com.example.board.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * @author Jason_Jiang
 *
 */
@Mapper
public interface WorkloadDao {

	int getWorkloadsByParentCount(@Param("parent") String parent);
	
}
