package com.example.board.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.example.board.entity.User;

/**
 *  
 * @author JZH
 *
 */
@Mapper
public interface UserDao {

	/**
	 * 根据id获取所有用户
	 * @param ids
	 * @return
	 */
	List<User> getUsersByIds(List<String> ids);
	
}
