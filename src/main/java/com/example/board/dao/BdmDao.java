package com.example.board.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.example.board.entity.BudgetManagement;

/**
 * 
 * @author Jason_Jiang
 *
 */
@Mapper
public interface BdmDao {

	/**
	 * 
	 * @param page
	 * @param rows
	 * @return
	 */
	List<BudgetManagement> getBdms(@Param("page") int page, @Param("rows") int rows);
	
	int getBdmsCount();
	
	Map<String, Object> getBdmByDept(@Param("dept") String dept);
}
