package com.example.board.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.example.board.entity.EleForm;

/**
 * 电子需求单
 * @author Jason_Jiang
 *
 */
@Mapper
public interface EleFormDao {
	
	/**
	 * 分页获取电子需求单列表
	 * @param page
	 * @param rows
	 * @return
	 */
	List<EleForm> getEleForms(@Param("code") String code, @Param("businessName") String businessName, @Param("stateNow") String stateNow, @Param("page") int page, @Param("rows") int rows);
	
	/**
	 * 获取电子需求单总数
	 * @return
	 */
	int getEleFormsCount(@Param("code") String code, @Param("businessName") String businessName, @Param("stateNow") String stateNow);
	
	/**
	 * 根据ID获取电子需求单详细信息
	 * @param id
	 * @return
	 */
	EleForm getEleFormById(@Param("id") String id);
	
	/**
	 * 根据预算管理获取需求单
	 * @param id
	 * @param page
	 * @param rows
	 * @return
	 */
	List<Map<String, Object>> getFormsByBdm(@Param("title") String id, @Param("page") int page, @Param("rows") int rows);
	
	/**
	 * 预算管理下的需求单数量
	 * @param id
	 * @return
	 */
	int getFormsCountByBdm(@Param("title") String id);
	
	/**
	 * 获取首页统计数据
	 * @param rows
	 * @return
	 */
	List<Map<String, Object>> createCharts(@Param("rows") int rows);
	
}
