package com.example.board.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.example.board.entity.Clause;

/**
 * 需求条目
 * @author JZH
 *
 */
@Mapper
public interface ClauseDao {

	/**
	 * 分页根据电子需求单获取对应的条目列表
	 * @param parent 电子需求单ID
	 * @param page
	 * @param rows
	 * @return
	 */
	List<Clause> getClausesByParent(@Param("parent") String parent, @Param("page") int page, @Param("rows") int rows);
	
	/**
	 * 根据电子需求单获取条目数量
	 * @return
	 */
	int getClausesByParentCount(@Param("parent") String parent);
	
	Clause getClauseById(@Param("id") String id);
	

	
}
