package com.example.board.entity;

import java.io.Serializable;

/**
 * 预算管理
 * @author Jason_Jiang
 *
 */
public class BudgetManagement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String author;
	
	private String createTime;
	
	/**
	 * 标题
	 */
	private String title;
	
	/**
	 * 录入人
	 */
	private String writer;
	
	/**
	 * 录入部门
	 */
	private String dept;
	
	/**
	 * 年度
	 */
	private String year;
	
	/**
	 * 预算人月单价
	 */
	private double budget;
	
	/**
	 * 预算总量
	 */
	private double budgetAll;
	
	/**
	 * 可用量
	 */
	private double budgetCan;
	
	/**
	 * 冻结量
	 */
	private double budgetLock;
	
	/**
	 * 已用量
	 */
	private double budgetHave;
	
	/**
	 * 签报编号
	 */
	private String signCode;
	
	/**
	 * 签报批准时间
	 */
	private String signTime;
	
	private int eFormCount;
	

	public int geteFormCount() {
		return eFormCount;
	}

	public void seteFormCount(int eFormCount) {
		this.eFormCount = eFormCount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public double getBudgetAll() {
		return budgetAll;
	}

	public void setBudgetAll(double budgetAll) {
		this.budgetAll = budgetAll;
	}

	public double getBudgetCan() {
		return budgetCan;
	}

	public void setBudgetCan(double budgetCan) {
		this.budgetCan = budgetCan;
	}

	public double getBudgetLock() {
		return budgetLock;
	}

	public void setBudgetLock(double budgetLock) {
		this.budgetLock = budgetLock;
	}

	public double getBudgetHave() {
		return budgetHave;
	}

	public void setBudgetHave(double budgetHave) {
		this.budgetHave = budgetHave;
	}

	public String getSignCode() {
		return signCode;
	}

	public void setSignCode(String signCode) {
		this.signCode = signCode;
	}

	public String getSignTime() {
		return signTime;
	}

	public void setSignTime(String signTime) {
		this.signTime = signTime;
	}

	public BudgetManagement() {
		super();
	}

	@Override
	public String toString() {
		return "BudgetManagement [id=" + id + ", author=" + author + ", createTime=" + createTime + ", title=" + title
				+ ", writer=" + writer + ", dept=" + dept + ", year=" + year + ", budget=" + budget + ", budgetAll="
				+ budgetAll + ", budgetCan=" + budgetCan + ", budgetLock=" + budgetLock + ", budgetHave=" + budgetHave
				+ ", signCode=" + signCode + ", signTime=" + signTime + "]";
	}
	
	
	
}
