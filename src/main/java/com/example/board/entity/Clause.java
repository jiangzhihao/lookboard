package com.example.board.entity;

import java.io.Serializable;

/**
 * 需求条目
 * @author JZH
 *
 */
public class Clause implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String parent;
	
	/**
	 * 系统架构影响分析
	 */
	private String framingAnalysis; 
	
	/**
	 *  回退状态
	 */
	private String status;
	
	/**
	 * 验收状态
	 */
	private String checkStatus;
	
	/**
	 * 是否是完整方案
	 */
	private String completeSolution;
	
	/**
	 * 是否是需求变更
	 */
	private String demandChange;
	
	/**
	 * 创建时间
	 */
	private String createTime;
	
	/**
	 * 需求类型
	 */
	private String demandType;
	
	/**
	 * 需求提出原因
	 */
	private String reason;
	
	/**
	 * 计划评估完成时间
	 */
	private String planAssessFinishedTime;
	
	/**
	 * 条目编号
	 */
	private String code;
	
	/**
	 * 紧迫程度
	 */
	private String degreeOfUrgency;
	
	/**
	 * 期望上线时间
	 */
	private String expectOnlineTime;
	
	/**
	 * 主题
	 */
	private String subject;
	
	/**
	 * 提出部门
	 */
	private String dept;
	
	/**
	 * 是否是X行项目组提出
	 */
	private String isX;
	
	/**
	 * 描述
	 */
	private String info;
	
	/**
	 * 应用规划备注
	 */
	private String planRemarks;
	
	/**
	 * 是否是4W1H
	 */
	private String is4w1h;
	
	private String whWhat;
	
	private String whWho;
	
	private String whHow;
	
	private String whWhere;
	
	private String Wwhen;
	
	/**
	 * 需求提出部门负责人
	 */
	private String proposeLeader;
	
	/**
	 * 设计部门负责人
	 */
	private String designLeader;
	
	/**
	 * 开发公司负责人
	 */
	private String developLeader;
	
	/**
	 * 实现平台
	 */
	private String platform;
	
	/**
	 * 实现系统
	 */
	private String system;
	
	/**
	 * 实现子系统
	 */
	private String subsystem;
	
	/**
	 * 实现实例
	 */
	private String instance;
	
	/**
	 * 实现模块
	 */
	private String module;
	
	/**
	 * 计划上线版本
	 */
	private String planVersion;
	
	/**
	 * 当前版本
	 */
	private String currentVersion;
	
	/**
	 * 签报版本
	 */
	private String signVersion;
	
	/**
	 * 外包投入量
	 */
	private double wbInput;
	
	/**
	 * 中汇投入量
	 */
	private double zhInput;
	
	/**
	 * 回退凉
	 */
	private double runBackInput;
	
	

	public Clause() {
		super();
	}
	
	

	public Clause(String id, String parent, String framingAnalysis, String status, String checkStatus,
			String completeSolution, String demandChange, String createTime, String demandType, String reason,
			String planAssessFinishedTime, String code, String degreeOfUrgency, String expectOnlineTime, String subject,
			String dept, String isX, String info, String planRemarks, String is4w1h, String whWhat, String whWho,
			String whHow, String whWhere, String wwhen, String proposeLeader, String designLeader, String developLeader,
			String platform, String system, String subsystem, String instance, String module, String planVersion,
			String currentVersion, String signVersion, double wbInput, double zhInput, double runBackInput) {
		super();
		this.id = id;
		this.parent = parent;
		this.framingAnalysis = framingAnalysis;
		this.status = status;
		this.checkStatus = checkStatus;
		this.completeSolution = completeSolution;
		this.demandChange = demandChange;
		this.createTime = createTime;
		this.demandType = demandType;
		this.reason = reason;
		this.planAssessFinishedTime = planAssessFinishedTime;
		this.code = code;
		this.degreeOfUrgency = degreeOfUrgency;
		this.expectOnlineTime = expectOnlineTime;
		this.subject = subject;
		this.dept = dept;
		this.isX = isX;
		this.info = info;
		this.planRemarks = planRemarks;
		this.is4w1h = is4w1h;
		this.whWhat = whWhat;
		this.whWho = whWho;
		this.whHow = whHow;
		this.whWhere = whWhere;
		Wwhen = wwhen;
		this.proposeLeader = proposeLeader;
		this.designLeader = designLeader;
		this.developLeader = developLeader;
		this.platform = platform;
		this.system = system;
		this.subsystem = subsystem;
		this.instance = instance;
		this.module = module;
		this.planVersion = planVersion;
		this.currentVersion = currentVersion;
		this.signVersion = signVersion;
		this.wbInput = wbInput;
		this.zhInput = zhInput;
		this.runBackInput = runBackInput;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getFramingAnalysis() {
		return framingAnalysis;
	}

	public void setFramingAnalysis(String framingAnalysis) {
		this.framingAnalysis = framingAnalysis;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}

	public String getCompleteSolution() {
		return completeSolution;
	}

	public void setCompleteSolution(String completeSolution) {
		this.completeSolution = completeSolution;
	}

	public String getDemandChange() {
		return demandChange;
	}

	public void setDemandChange(String demandChange) {
		this.demandChange = demandChange;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getDemandType() {
		return demandType;
	}

	public void setDemandType(String demandType) {
		this.demandType = demandType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getPlanAssessFinishedTime() {
		return planAssessFinishedTime;
	}

	public void setPlanAssessFinishedTime(String planAssessFinishedTime) {
		this.planAssessFinishedTime = planAssessFinishedTime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDegreeOfUrgency() {
		return degreeOfUrgency;
	}

	public void setDegreeOfUrgency(String degreeOfUrgency) {
		this.degreeOfUrgency = degreeOfUrgency;
	}

	public String getExpectOnlineTime() {
		return expectOnlineTime;
	}

	public void setExpectOnlineTime(String expectOnlineTime) {
		this.expectOnlineTime = expectOnlineTime;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getIsX() {
		return isX;
	}

	public void setIsX(String isX) {
		this.isX = isX;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getPlanRemarks() {
		return planRemarks;
	}

	public void setPlanRemarks(String planRemarks) {
		this.planRemarks = planRemarks;
	}

	public String getIs4w1h() {
		return is4w1h;
	}

	public void setIs4w1h(String is4w1h) {
		this.is4w1h = is4w1h;
	}

	public String getWhWhat() {
		return whWhat;
	}

	public void setWhWhat(String whWhat) {
		this.whWhat = whWhat;
	}

	public String getWhWho() {
		return whWho;
	}

	public void setWhWho(String whWho) {
		this.whWho = whWho;
	}

	public String getWhHow() {
		return whHow;
	}

	public void setWhHow(String whHow) {
		this.whHow = whHow;
	}

	public String getWhWhere() {
		return whWhere;
	}

	public void setWhWhere(String whWhere) {
		this.whWhere = whWhere;
	}

	public String getWwhen() {
		return Wwhen;
	}

	public void setWwhen(String wwhen) {
		Wwhen = wwhen;
	}

	public String getProposeLeader() {
		return proposeLeader;
	}

	public void setProposeLeader(String proposeLeader) {
		this.proposeLeader = proposeLeader;
	}

	public String getDesignLeader() {
		return designLeader;
	}

	public void setDesignLeader(String designLeader) {
		this.designLeader = designLeader;
	}

	public String getDevelopLeader() {
		return developLeader;
	}

	public void setDevelopLeader(String developLeader) {
		this.developLeader = developLeader;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getSubsystem() {
		return subsystem;
	}

	public void setSubsystem(String subsystem) {
		this.subsystem = subsystem;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getPlanVersion() {
		return planVersion;
	}

	public void setPlanVersion(String planVersion) {
		this.planVersion = planVersion;
	}

	public String getCurrentVersion() {
		return currentVersion;
	}

	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}

	public String getSignVersion() {
		return signVersion;
	}

	public void setSignVersion(String signVersion) {
		this.signVersion = signVersion;
	}

	public double getWbInput() {
		return wbInput;
	}

	public void setWbInput(double wbInput) {
		this.wbInput = wbInput;
	}

	public double getZhInput() {
		return zhInput;
	}

	public void setZhInput(double zhInput) {
		this.zhInput = zhInput;
	}

	public double getRunBackInput() {
		return runBackInput;
	}

	public void setRunBackInput(double runBackInput) {
		this.runBackInput = runBackInput;
	}

	@Override
	public String toString() {
		return "Clause [id=" + id + ", parent=" + parent + ", framingAnalysis=" + framingAnalysis + ", status=" + status
				+ ", checkStatus=" + checkStatus + ", completeSolution=" + completeSolution + ", demandChange="
				+ demandChange + ", createTime=" + createTime + ", demandType=" + demandType + ", reason=" + reason
				+ ", planAssessFinishedTime=" + planAssessFinishedTime + ", code=" + code + ", degreeOfUrgency="
				+ degreeOfUrgency + ", expectOnlineTime=" + expectOnlineTime + ", subject=" + subject + ", dept=" + dept
				+ ", isX=" + isX + ", info=" + info + ", planRemarks=" + planRemarks + ", is4w1h=" + is4w1h
				+ ", whWhat=" + whWhat + ", whWho=" + whWho + ", whHow=" + whHow + ", whWhere=" + whWhere + ", Wwhen="
				+ Wwhen + ", proposeLeader=" + proposeLeader + ", designLeader=" + designLeader + ", developLeader="
				+ developLeader + ", platform=" + platform + ", system=" + system + ", subsystem=" + subsystem
				+ ", instance=" + instance + ", module=" + module + ", planVersion=" + planVersion + ", currentVersion="
				+ currentVersion + ", signVersion=" + signVersion + ", wbInput=" + wbInput + ", zhInput=" + zhInput
				+ ", runBackInput=" + runBackInput + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
}
