package com.example.board.entity;

import java.io.Serializable;

/**
 * 电子需求单
 * @author Jason_Jiang
 *
 */
public class EleForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	/**
	 * 申请单号
	 */
	private String code;
	
	/**
	 * 创建时间
	 */
	private String createTime;
	
	/**
	 * 申请人
	 */
	private String proposer;
	
	/**
	 * 申请部门
	 */
	private String dept;
	
	/**
	 * 当前节点
	 */
	private String stateNow;
	
	/**
	 * 当前审批人
	 */
	private String auditors;
	
	/**
	 * 业务类型
	 */
	private String businessType;
	
	/**
	 * 业务名称
	 */
	private String businessName;
	
	/**
	 * 主体系统
	 */
	private String mainSystem;
	
	/**
	 * 配合系统
	 */
	private String supporterSystem;
	
	/**
	 * 需求部门评估人员
	 */
	private String demandEstimator;
	
	/**
	 * 设计部门评估人员
	 */
	private String designEstimator;
	
	/**
	 * 开发公司评估人员
	 */
	private String developEstimator;
	
	/**
	 * 运维部门评估人员
	 */
	private String operationEstimator;
	
	/**
	 * 申请时间
	 */
	private String applyTime;

	/**
	 * 是否是IT前置项目
	 */
	private String preIt;
	
	/**
	 * 主体负责人
	 */
	private String grobalLeader;
	
	/**
	 * 可用量
	 */
	private double canUse;
	
	/**
	 * 已用量
	 */
	private double haveUse;
	
	/**
	 * 冻结量
	 */
	private double lockUse;
	
	/**
	 * 中汇工作量
	 */
	private double workloadZh;
	
	/**
	 * 外包工作量
	 */
	private double workloadWb;
	
	/**
	 * 预算签报编号
	 */
	private String budgetCode;
	
	/**
	 * 预算来源
	 */
	private String budgetFrom;
	
	
	

	public EleForm() {
		super();
	}
	
	
	/**
	 * 
	 * @param id
	 * @param createTime
	 * @param proposer
	 * @param dept
	 * @param stateNow
	 * @param auditors
	 * @param businessType
	 * @param businessName
	 * @param mainSystem
	 * @param supporterSystem
	 * @param demandEstimator
	 * @param designEstimator
	 * @param developEstimator
	 * @param operationEstimator
	 * @param applyTime
	 * @param preIt
	 * @param grobalLeader
	 * @param canUse
	 * @param haveUse
	 * @param lockUse
	 * @param workloadZh
	 * @param workloadWb
	 * @param budgetCode
	 * @param budgetFrom
	 */
	public EleForm(String id, String createTime, String proposer, String dept, String stateNow, String auditors,
			String businessType, String businessName, String mainSystem, String supporterSystem, String demandEstimator,
			String designEstimator, String developEstimator, String operationEstimator, String applyTime, String preIt,
			String grobalLeader, double canUse, double haveUse, double lockUse, double workloadZh, double workloadWb,
			String budgetCode, String budgetFrom) {
		super();
		this.id = id;
		this.createTime = createTime;
		this.proposer = proposer;
		this.dept = dept;
		this.stateNow = stateNow;
		this.auditors = auditors;
		this.businessType = businessType;
		this.businessName = businessName;
		this.mainSystem = mainSystem;
		this.supporterSystem = supporterSystem;
		this.demandEstimator = demandEstimator;
		this.designEstimator = designEstimator;
		this.developEstimator = developEstimator;
		this.operationEstimator = operationEstimator;
		this.applyTime = applyTime;
		this.preIt = preIt;
		this.grobalLeader = grobalLeader;
		this.canUse = canUse;
		this.haveUse = haveUse;
		this.lockUse = lockUse;
		this.workloadZh = workloadZh;
		this.workloadWb = workloadWb;
		this.budgetCode = budgetCode;
		this.budgetFrom = budgetFrom;
	}



	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getProposer() {
		return proposer;
	}

	public void setProposer(String proposer) {
		this.proposer = proposer;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getStateNow() {
		return stateNow;
	}

	public void setStateNow(String stateNow) {
		this.stateNow = stateNow;
	}

	public String getAuditors() {
		return auditors;
	}

	public void setAuditors(String auditors) {
		this.auditors = auditors;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getMainSystem() {
		return mainSystem;
	}

	public void setMainSystem(String mainSystem) {
		this.mainSystem = mainSystem;
	}

	public String getSupporterSystem() {
		return supporterSystem;
	}

	public void setSupporterSystem(String supporterSystem) {
		this.supporterSystem = supporterSystem;
	}

	public String getDemandEstimator() {
		return demandEstimator;
	}

	public void setDemandEstimator(String demandEstimator) {
		this.demandEstimator = demandEstimator;
	}

	public String getDesignEstimator() {
		return designEstimator;
	}

	public void setDesignEstimator(String designEstimator) {
		this.designEstimator = designEstimator;
	}

	public String getDevelopEstimator() {
		return developEstimator;
	}

	public void setDevelopEstimator(String developEstimator) {
		this.developEstimator = developEstimator;
	}

	public String getOperationEstimator() {
		return operationEstimator;
	}

	public void setOperationEstimator(String operationEstimator) {
		this.operationEstimator = operationEstimator;
	}

	public String getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(String applyTime) {
		this.applyTime = applyTime;
	}

	public String getPreIt() {
		return preIt;
	}

	public void setPreIt(String preIt) {
		this.preIt = preIt;
	}

	public String getGrobalLeader() {
		return grobalLeader;
	}

	public void setGrobalLeader(String grobalLeader) {
		this.grobalLeader = grobalLeader;
	}

	public double getCanUse() {
		return canUse;
	}

	public void setCanUse(double canUse) {
		this.canUse = canUse;
	}

	public double getHaveUse() {
		return haveUse;
	}

	public void setHaveUse(double haveUse) {
		this.haveUse = haveUse;
	}

	public double getLockUse() {
		return lockUse;
	}

	public void setLockUse(double lockUse) {
		this.lockUse = lockUse;
	}

	public double getWorkloadZh() {
		return workloadZh;
	}

	public void setWorkloadZh(double workloadZh) {
		this.workloadZh = workloadZh;
	}

	public double getWorkloadWb() {
		return workloadWb;
	}

	public void setWorkloadWb(double workloadWb) {
		this.workloadWb = workloadWb;
	}

	public String getBudgetCode() {
		return budgetCode;
	}

	public void setBudgetCode(String budgetCode) {
		this.budgetCode = budgetCode;
	}

	public String getBudgetFrom() {
		return budgetFrom;
	}

	public void setBudgetFrom(String budgetFrom) {
		this.budgetFrom = budgetFrom;
	}


	@Override
	public String toString() {
		return "EleForm [id=" + id + ", createTime=" + createTime + ", proposer=" + proposer + ", dept=" + dept
				+ ", stateNow=" + stateNow + ", auditors=" + auditors + ", businessType=" + businessType
				+ ", businessName=" + businessName + ", mainSystem=" + mainSystem + ", supporterSystem="
				+ supporterSystem + ", demandEstimator=" + demandEstimator + ", designEstimator=" + designEstimator
				+ ", developEstimator=" + developEstimator + ", operationEstimator=" + operationEstimator
				+ ", applyTime=" + applyTime + ", preIt=" + preIt + ", grobalLeader=" + grobalLeader + ", canUse="
				+ canUse + ", haveUse=" + haveUse + ", lockUse=" + lockUse + ", workloadZh=" + workloadZh
				+ ", workloadWb=" + workloadWb + ", budgetCode=" + budgetCode + ", budgetFrom=" + budgetFrom + "]";
	}
	
	
	
}
