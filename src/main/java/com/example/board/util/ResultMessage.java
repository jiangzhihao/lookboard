package com.example.board.util;


import java.io.Serializable;

/**
 * @author Fandy
 * @project booms
 * @description: result message for response
 * @create 2018-04-07 16:18
 **/
public class ResultMessage<T> implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean ok;

    private T data;

    private String code;

    private String message;

    private ResultMessage(boolean ok, T data) {
        this.ok = ok;
        this.data = data;
    }

    private ResultMessage(boolean ok, String code, String message) {
        this.ok = ok;
        this.code = code;
        this.message = message;
    }

    public static <T> ResultMessage<T> ok(T data) {
        return new ResultMessage<>(true, data);
    }

    public static <T> ResultMessage<T> error(String code, String message) {
        return new ResultMessage<>(false, code, message);
    }


    public boolean isOk() {
        return ok;
    }

    public boolean getOk() {
        return ok;
    }

    public T getData() {
        return data;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
