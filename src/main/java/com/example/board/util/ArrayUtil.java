package com.example.board.util;

/**
 * 
 * @author Jason_Jiang
 *
 */
public class ArrayUtil {

	/**
	 * 反转数组
	 * @param array
	 */
	public static void reverse(Object[] array) {
        if (array == null) {
            return;
        }
        int i = 0;
        int j = array.length - 1;
        Object tmp;
        while (j > i) {
            tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;
            j--;
            i++;
        }
    }
	
}
