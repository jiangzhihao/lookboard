package com.example.board.config;



import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 扩展SpringMVC的功能，在以前版本需要编写配置类并继承WebMvcConfigurerAdapter
 * 现在可以直接实现WebMvcConfigurer接口
 * 
 * @author JZH
 *
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {


	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		// TODO Auto-generated method stub
		registry.addViewController("/budget_management.html").setViewName("budget_management.html");
	}
	
	
	
	
}
